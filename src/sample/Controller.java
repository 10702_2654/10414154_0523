package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button btnWorking;
    public ProgressBar pbWorking;
    public ImageView iv;

    public void onWorking(ActionEvent actionEvent) {
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        for (int i = 0; i < 4;i++){
            int j = i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource("/resourse/".concat(String.valueOf(j)).concat(".png")).toString()));
                        }
                    })
            );

        }
        timeline.play();

        Task task  = new Task<Void>(){

            @Override
            protected Void call() throws Exception {
                final  int max = 100;
                for (int i = 1; i < max;i++){
                    Thread.sleep(100);
                    updateProgress(i,max);
                }
                return null;
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                timeline.stop();
                iv.setVisible(false);
                pbWorking.setVisible(false);

            }

            @Override
            protected void failed() {
                super.failed();
            }



        };

        pbWorking.progressProperty().bind(task.progressProperty());

        new Thread(task).start();
        pbWorking.setVisible(true);
        new  Thread(task).start();

        List<String> imagepath = new ArrayList<>();




    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pbWorking.setProgress(ProgressBar.INDETERMINATE_PROGRESS);
        pbWorking.setVisible(false);

        //iv.setImage(new Image(getClass().getResource("/resourse/0.png").toString()));


    }
}


